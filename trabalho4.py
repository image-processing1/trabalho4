import os
import sys
from skimage import io
import numpy as np
import cv2
import matplotlib.pyplot as plt
plt.style.use('ggplot')

STR_EL1 = np.ones((1, 100), dtype=np.uint8)
STR_EL2 = np.ones((200, 1), dtype=np.uint8)
STR_EL3 = np.ones((1, 30), dtype=np.uint8)
STR_EL4 = np.ones((10, 12), dtype=np.uint8)


def apply_morph(img, kernel, morph='dilate', iterations=1):
    if(morph == 'dilate'):
        return cv2.dilate(img, kernel, iterations)
    if(morph == 'erode'):
        return cv2.erode(img, kernel, iterations)
    if(morph == 'opening'):
        return cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)
    if(morph == 'closing'):
        return cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel)
    return img


def get_connected_components(img):
    '''
    Returns a set of bounding boxes that holds the connected components
    '''
    contours, hierarchy = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    contours_poly = [None]*len(contours)
    boundRect = [None]*len(contours)
    for i, c in enumerate(contours):
        contours_poly[i] = cv2.approxPolyDP(c, 3, closed=True)
        boundRect[i] = cv2.boundingRect(contours_poly[i])

    return boundRect


def count_black(arr):
    return np.count_nonzero(arr==1)


def get_ratio_black_pixels(img, bbox):
    '''
    Returns the ratio of black pixels inside the bounding box
    '''
    x,y,w,h = bbox
    inside = img[y:y+h,x:x+w]/255

    n_pixels = inside.shape[0]*inside.shape[1]
    n_black = count_black(inside)
    ratio = n_black/n_pixels

    return ratio


def count_row_transitions(arr):
    count = 0
    for row in arr:
        last_pix = row[0]
        for pix in row:
            if(last_pix != pix):
                count+=1
                last_pix = pix
    return count


def get_ratio_transitions(img, bbox):
    '''
    Returns the ration of transitions between black and white inside the
    bounding box
    '''
    x,y,w,h = bbox
    inside = img[y:y+h,x:x+w]/255
    n_black = count_black(inside)
    n_vertical = count_row_transitions(inside)
    n_horizontal = count_row_transitions(inside.T) #transpose array
    if(n_black>0):
        ratio = (n_vertical+n_horizontal)/n_black
    else:
        return 0
    return ratio


def is_text(black_ratio, trans_ratio):
    if(black_ratio > trans_ratio):
        if(black_ratio > 0.1 and black_ratio < 0.9 and trans_ratio < 0.4):
            return True


def draw_bboxes(img, bboxes, show=True, save_path=None, colour=0):
    for bbox in bboxes:
        x,y,w,h = bbox
        cv2.rectangle(img, (x,y), (x+w, y+h), (0,0,0), 2)

    if(show):
        io.imshow(img)
        io.show()

    if(save_path):
        io.imsave(save_path, img)


if __name__ == '__main__':
    assert len(sys.argv)==2, "You should use image path as first argument"
    path = sys.argv[1]
    img = io.imread(path)
    print("Shape: ", img.shape, " Type: ", img.dtype)
    os.makedirs('results/',exist_ok=True)
    #io.imsave('results/bitmap.png', img)

    neg_img = ~img #opencv logic is inverted
    #io.imsave('results/neg_img.png', neg_img)

    img_dil1 = apply_morph(neg_img, STR_EL1, 'dilate') #step 1
    io.imsave('results/dilation1_step1.pbm', img_dil1)
    img_ero1 = apply_morph(img_dil1, STR_EL1, 'erode') #step 2
    io.imsave('results/erosion1_step2.pbm', img_ero1)

    img_dil2 = apply_morph(neg_img, STR_EL2, 'dilate') #step 3
    io.imsave('results/dilation2_step3.pbm', img_dil2)
    img_ero2 = apply_morph(img_dil2, STR_EL2, 'erode') #step 4
    io.imsave('results/erosion2_step4.pbm', img_ero2)

    _img = img_ero1 & img_ero2 #step 5
    io.imsave('results/intersection_step5.pbm', _img)

    img_clos = apply_morph(_img, STR_EL3, 'closing') #step 6
    io.imsave('results/closing_step6.pbm', img_clos)

    bboxes = get_connected_components(img_clos) #step 7
    print("number of connected components: ", len(bboxes))
    draw_bboxes(~img_clos.copy(), bboxes, show=False, save_path='results/connected_components_step7.pbm')

    text_bboxes = []
    all_black_ratio = []
    all_trans_ratio = []
    for bbox in bboxes:
        black_ratio = get_ratio_black_pixels(img_clos, bbox) #step 8a
        all_black_ratio.append(black_ratio)
        trans_ratio = get_ratio_transitions(img_clos, bbox) #step 8b
        all_trans_ratio.append(trans_ratio)

        if(is_text(black_ratio, trans_ratio)): #step 9
            text_bboxes.append(bbox)

    plt.hist(all_black_ratio, label='Pixels pretos')
    plt.hist(all_trans_ratio, label='Pixels de transição')
    plt.legend()
    plt.xlabel('Proporção')
    plt.ylabel('Frequência')
    plt.savefig('results/ratio.png')

    print('number of rows ', len(text_bboxes))
    draw_bboxes(img.copy(), text_bboxes, show=False, save_path='results/rows_bboxes_step9.pbm')

    #step 10
    words_bboxes = []
    for bbox in text_bboxes:
        x,y,w,h = bbox
        line = neg_img[y:y+h,x:x+w]
        line = apply_morph(line, STR_EL4, 'closing')
        words = get_connected_components(line)
        words = [(bbox[0]+x, bbox[1]+y, bbox[2], bbox[3]) for bbox in words]
        words_bboxes+=words

    print('number of words ', len(words_bboxes))
    draw_bboxes(img.copy(), words_bboxes, show=False, save_path='results/words_bboxes_step10.pbm')
